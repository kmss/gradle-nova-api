package kmss.api;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.inquira.client.serviceclient.IQServiceClientException;

// Plain old Java Object it does not extend as class or implements
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation.
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML.

// The browser requests per default the HTML MIME type.

//Sets the path to base URL + /hello
@Path("/hello")
public class Hello {
	


  // This method is called if TEXT_PLAIN is request
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String sayPlainTextHello() throws IOException, IQServiceClientException {
	  return "HI!";
		
  }

  
  // This method is called if TEXT_PLAIN is request
  @POST
  @Produces(MediaType.TEXT_PLAIN)
  public String sayPlainTextHello1() {
    return "Hello POST";
  }

  
  // This method is called if XML is request
  @GET
  @Produces(MediaType.TEXT_XML)
  public String sayXMLHello() {
    return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey" + "</hello>";
  }

  // This method is called if HTML is request
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String sayHtmlHello() throws IOException, IQServiceClientException {
	  
	  UserAPI api = new UserAPI();
		System.out.println("hello!");
		
		return api.getRoleByRefKey("KM_SOLUTIONS").MODE_CLASS_NAME;
		
		/*
    return "<html> " + "<title>" + "Hello Jersey" + "</title>"
        + "<body><h1>" + "Hello Jersey" + "</body></h1>" + "</html> ";
        */
  }

}