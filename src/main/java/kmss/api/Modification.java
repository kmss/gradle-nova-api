package kmss.api;

import java.util.List;
import java.util.Map;

public class Modification {
	
	String action;
	String status;
	
	List<String> users;
	List<String> addRoles;
	List<String> addViews;
	List<String> addSkills;
	List<String> addWTeams;
	Map<String, String> results;
	String userType;
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Map<String, String> getResults() {
		return results;
	}
	public void setResults(Map<String, String> results) {
		this.results = results;
	}
	public void setAddSkills(List<String> addSkills) {
		this.addSkills = addSkills;
	}
	
	public List<String> getAddWTeams() {
		return addWTeams;
	}
	public void setAddWTeams(List<String> addWTeams) {
		this.addWTeams = addWTeams;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<String> getAddSkills() {
		return addSkills;
	}
	public void setAddSkill(List<String> addSkill) {
		this.addSkills = addSkill;
	}
	public List<String> getAddTeams() {
		return addTeams;
	}
	public void setAddTeams(List<String> addTeams) {
		this.addTeams = addTeams;
	}
	List<String> addTeams;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<String> getUsers() {
		return users;
	}
	public void setUsers(List<String> users) {
		this.users = users;
	}
	public List<String> getAddRoles() {
		return addRoles;
	}
	public void setAddRoles(List<String> addRoles) {
		this.addRoles = addRoles;
	}
	public List<String> getAddViews() {
		return addViews;
	}
	public void setAddViews(List<String> addViews) {
		this.addViews = addViews;
	}
	public String toString() {
		return "User: " + this.getUsers() + " Action: " + this.getAction() + " Roles: " + this.getAddRoles() + " Views: " + this.getAddViews();
	}

}
