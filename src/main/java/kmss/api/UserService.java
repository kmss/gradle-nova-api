package kmss.api;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UserService {
	
	@Path("{userID}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getRole(@PathParam("userID") String ref) 
	{
		System.out.println("made it!");

		try {
			UserAPI api = new UserAPI();
			return api.getUser(ref);
				
			//return api.getRoleByRefKey(ref).getRecordID();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}
	
	
	@Path("/create")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Modification createUser(Modification input) 
	{
		try {
			UserAPI api = new UserAPI();
			return null;
			//return api.createUsers().getLogin();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	
	@Path("/getUsers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(Modification input) 
	{
		try {
			UserAPI api = new UserAPI();
			return api.getUser(input.getUsers().get(0));
			//return api.createUsers().getLogin();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "error";
		}
	}
	
	
	/*
	@POST
	@Path("/makeWeb/{webID}")
	public Map<String, String> makeWebUser(@PathParam("webID") String u) throws IOException {
		UserAPI api = new UserAPI();
		Map<String, String> result = new HashMap<String, String>();
		String a = api.createWebUser(u);
		result.put(u, a);
		return result;
	}
	*/
	
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)	
	//@Consumes(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public Modification updateUser( Modification modObj) throws IOException 
	{
			System.out.println("made it!");

			UserAPI api = new UserAPI();
			//System.out.println(modObj.getUser());
			String s = "Updating..." + modObj.getUsers();
			System.out.println(s);
			
			try {
				
				if (modObj.getUserType().equals("web")) {
					return api.createWebUser(modObj);
					
				}
				else if (modObj.getAction().equals("ADD")) {
					return api.createNewUser(modObj);
					//return Response.status(200).entity("Added new user: " + modObj.getUser()).build();
				}
				
				else if (modObj.getAction().equals("MODIFY")) {
					Modification b = api.updateExistingUser(modObj);
					//System.out.println(b);
					return b;
					//return Response.status(200).entity("Modified user: " + modObj.getUser()).build();

				}
				
				else if (modObj.getAction().equals("DEACTIVATE")) {
					return api.deactivateUser(modObj);
					//return Response.status(200).entity("Deacticated user: " + modObj.getUser()).build();
				}
			
				return null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("RETURN 500!");
			//return "error";
			modObj.setStatus("Error");
			return modObj;
			//return Response.status(500).entity("Error with this user: " + modObj.getUser()).build();
		}

	}

}
