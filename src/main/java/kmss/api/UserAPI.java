package kmss.api;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.inquira.client.serviceclient.IQServiceClient;
import com.inquira.client.serviceclient.IQServiceClientException;
import com.inquira.client.serviceclient.IQServiceClientManager;
import com.inquira.client.serviceclient.request.IQUserRequest;
import com.inquira.im.ito.CategoryKeyITO;
import com.inquira.im.ito.LocaleKeyITO;
import com.inquira.im.ito.RatingKeyITO;
import com.inquira.im.ito.SecurityRoleKeyITO;
import com.inquira.im.ito.UserITO;
import com.inquira.im.ito.ViewKeyITO;
import com.inquira.im.ito.WorkTeamKeyITO;
import com.inquira.im.ito.impl.UserITOImpl;

public class UserAPI {
	
	private IQServiceClient client;
	
	public UserAPI() throws IOException
	{
		
		Properties prop = new Properties();
		InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties");
		
		//InputStream input = new FileInputStream("/src/main/java/config.properties");

		prop.load(input);
		
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");
		String url = prop.getProperty("url");

		try
		{
			this.client = IQServiceClientManager.connect(user, password, "KM", "KM", url, null, true);
		
		}
		
		catch (Exception e) 
		{
			System.out.print("Unable to connect to IMWS service");
			this.client = IQServiceClientManager.connect(user, password, "KM", "KM", url, null, true);

		}
	}
		
	
	public SecurityRoleKeyITO getRoleByRefKey(String refKey) throws IQServiceClientException{
		
		List<SecurityRoleKeyITO> allRoles = this.client.getSecurityRoleRequest().getSecurityRoleKeysForRepository("KM", 200);

		for(SecurityRoleKeyITO role: allRoles){
			if (role.getReferenceKey().equals(refKey)){
				System.out.println(role.MODE_CLASS_NAME);
				return role;
			}
		}
		return null;
		
	}
	
	public WorkTeamKeyITO getWorkTeamByRefKey(String refKey) throws IQServiceClientException{
		
		List<WorkTeamKeyITO> allTeams = this.client.getWorkTeamRequest().workTeamListKeyForRepository("KM", 200);

		for(WorkTeamKeyITO team: allTeams){
			if (team.getReferenceKey().equals(refKey)){
				return team;
			}
		}
		return null;
	}
	
	
	
	public String getUser(String userID) {
		IQUserRequest userRequest = client.getUserRequest();
		//Modification m = new Modification();

		try {
			UserITO myUser = userRequest.getUserByLogin(userID.toLowerCase());
			
			return myUser.toJSON();

		} catch (IQServiceClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	

	
	public Modification deactivateUser(Modification modObj) throws IOException, IQServiceClientException{
		
		Map<String, String> results = new HashMap<String, String>();

		IQUserRequest userRequest = client.getUserRequest();
		//userRequest.lockUser(u);							//locked vs inactive

			for (String u: modObj.getUsers()) {
				try{
				
		        	UserITO myUser = userRequest.getUserByLogin(u.toLowerCase());
		        	
	        		//userRequest.lockUser(u);

		        	if(myUser.getActive()){
		        		myUser.setActive(false);
			        	//System.out.println(myUser.getActive());
		        		userRequest.lockUser(u);
			        	//userRequest.updateUser(myUser);
		        		
		        		
		        	}
				results.put(u, "Success");

				}catch (IQServiceClientException e){
					e.printStackTrace();
					results.put(u, "Error");
				}
		}
		
		modObj.setResults(results);
		return modObj;
		
	}
	
	
	public Modification createWebUser(Modification modObj) {
		
		System.out.println("Web user making");
		Map<String, String> results = new HashMap<String, String>();
		IQUserRequest userRequest = this.client.getUserRequest();

		for(String userID: modObj.getUsers()) {

			try {
					
				UserITOImpl newUser = new UserITOImpl();
				newUser.setFirstName(userID + "_First");
				newUser.setLastName(userID + "_Last");
				newUser.setLogin(userID);
				newUser.setPassword("lol");
				newUser.setEmail(userID + "@rci.rogers.com");
				
				newUser.setAdminUser(false);
				newUser.setAlias("Web User Created by the API on: " + new Date().toString());
					
		    	newUser.setDefaultView(client.getViewRequest().getViewKeyByReferenceKey("KM"));
		    	newUser.setRepository(this.client.getRepositoryRequest().getRepositoryKeyByReferenceKey("KM"));

		    	newUser.setActive(true);
				LocaleKeyITO locale = client.getLocaleRequest().getLocaleKeyByLocaleCode("en_CA");
				newUser.setDefaultLocale(locale);
				userRequest.createUser(newUser);

				results.put(userID, "Success");
					
				}catch(Exception e) {
					
					e.printStackTrace();
					results.put(userID, "error");
				}
		}
		
		modObj.setResults(results);
		return modObj;
	}
	
	public Modification createNewUser(Modification modObj){
		
		Map<String, String> results = new HashMap<String, String>();

		for(String u: modObj.getUsers()) {
			try {
				UserITOImpl newUser = new UserITOImpl();
				newUser.setFirstName(u + "_First");
				newUser.setLastName(u + "_Last");
				newUser.setLogin(u);
				newUser.setPassword("lol");
				newUser.setEmail(u + "@rci.rogers.com");
				
				//CategoryKeyITO cat = client.getCategoryRequest().getCategoryByReferenceKey("EMPLOYEE");
		
				LocaleKeyITO locale = client.getLocaleRequest().getLocaleKeyByLocaleCode("en_CA");
				newUser.setDefaultLocale(locale);
				
				List<LocaleKeyITO> contentLocales = new ArrayList<LocaleKeyITO>();
				contentLocales.add(locale);
				
				newUser.setContentLocales(contentLocales);
				
				//ViewKeyITO v2 = client.getViewRequest().getViewKeyByReferenceKey("KM");
				List<ViewKeyITO> views = new ArrayList<ViewKeyITO>();
				if(modObj.getAddViews() != null) {
					for(String viewRef: modObj.getAddViews()) {
						views.add(client.getViewRequest().getViewKeyByReferenceKey(viewRef));
					}
				}
				//views.add(v2);
				
				newUser.setViews(views);
				
		    	newUser.setDefaultView(client.getViewRequest().getViewKeyByReferenceKey("KM"));
		
		    	List<CategoryKeyITO> skills = new ArrayList<CategoryKeyITO>();
		    	if(modObj.getAddSkills() != null) {
			    	for(String skillRef: modObj.getAddSkills()) {
			    		skills.add(client.getCategoryRequest().getCategoryKeyByReferenceKey(skillRef));
			    	}
		    	}
		    	newUser.setSkills(skills);
		    	
		    	
		    	List<RatingKeyITO> forms = new ArrayList<RatingKeyITO>();
		    	forms.add( client.getRatingRequest().getRatingDefinition("CONTENT_QUALITY") );
		    	newUser.setDataFormNotifications(forms);
		    	
		    	newUser.setCanReceiveEmailNotificationsForAssignedTasks(true);
		    	//newUser.setCanReceiveEmailNotificationsForTasksICanPerform(true);
		    	
		      	List<WorkTeamKeyITO> wTeams = new ArrayList<WorkTeamKeyITO>();
		    	if(modObj.getAddWTeams() != null) {
			    	for(String wTeam: modObj.getAddWTeams()) {
			    		wTeams.add(getWorkTeamByRefKey(wTeam));
			    	}
		    	}
		    	newUser.setWorkTeams(wTeams);
		    	
		    	
		    	List<SecurityRoleKeyITO> roles = new ArrayList<SecurityRoleKeyITO>();
		    	if(modObj.getAddRoles() != null) {
			    	for(String roleRef: modObj.getAddRoles()) {
			    		roles.add(getRoleByRefKey(roleRef));
			    	}
		    	}
		    	
		    	newUser.setSecurityRoles(roles);
		    	newUser.setAdminUser(true);			//console user
		    	newUser.setActive(true);
		    	newUser.setRepository(this.client.getRepositoryRequest().getRepositoryKeyByReferenceKey("KM"));
		    	
		    	newUser.setReportingUserGroup(this.client.getUserGroupRequest().getUserGroupKeyByReferenceKey("CUSTOMER_SERVICE"));
		
		    	newUser.setAlias("Created by API on: " + new Date().toString());
		
		    	
		    	/*
		    	newUser.setIsDefaultAdministrator(true);
		    	newUser.setDateAdded(new Date());
		
		    	newUser.setCanReceiveEmailNotificationsForAssignedTasks(true);
		    	newUser.setCanReceiveEmailNotificationsForTasksICanPerform(true);
		    	newUser.setDateModified(new Date());
		    	
		    	String uuid = UUID.randomUUID().toString().replace("-", "");
		        //System.out.println("uuid = " + uuid);
		    	
		    	newUser.setRecordID(uuid);
		    	*/
		    	
		    	//newUser.setExtendedProperties("");
		    	//newUser.setReputationPoints(4);
		    	    	
				IQUserRequest userRequest = this.client.getUserRequest();
				
				userRequest.createUser(newUser);
				//System.out.println( userRequest.getUserByLogin(generatedString.toLowerCase()).getLogin());
				//modObj.setStatus("Success");
				//return modObj;
				
				results.put(u, "Success");
			
			}
			catch(Exception e) {
	        	System.out.println("Exception Adding user: ");
	        	e.printStackTrace();
	        	
	        	results.put(u, "Error: User with ID already exists!");
	        	//modObj.setStatus("Error: User with ID already exists!");
	        	//return modObj;	
				
			}
			
		}
		modObj.setResults(results);
		return modObj;
		
	}
	
	
	
	//updates users with roles/views/categories
		public Modification updateExistingUser(Modification modObj) throws IOException, IQServiceClientException{

			Map<String, String> results = new HashMap<String, String>();
			IQUserRequest userRequest = client.getUserRequest();
			
			//client.getSecurityRoleRequest().getSecurityRoleDatasForUser(client.getUserRequest().getUserByLogin("andrew.tse")).get(0).
			
			//client.getSecurityRoleRequest().getS
			
			
			for (String u: modObj.getUsers()) {
				
			
	        try{	
	        	UserITO myUser = userRequest.getUserByLogin(u.toLowerCase());
	        	
	        	List<ViewKeyITO> views = new ArrayList<ViewKeyITO>();
	        	if(modObj.getAddViews() != null) {
		    		for(String viewRef: modObj.getAddViews()) {
		    			views.add(client.getViewRequest().getViewKeyByReferenceKey(viewRef));
		    		}
	        	}
	    		myUser.setViews(views);
	
	        	//SecurityRoleKeyITO role1 = getRoleByRefKey("HOME_BASE_SALES");
	        	List<SecurityRoleKeyITO> roles = new ArrayList<SecurityRoleKeyITO>();
	        	if (modObj.getAddRoles() != null) {
		        	for(String roleRef: modObj.getAddRoles()) {
		        		roles.add(getRoleByRefKey(roleRef));
		        	}
	        	}
	        	myUser.setSecurityRoles(roles);
	        	
	        	
	        	
	         	List<CategoryKeyITO> skills = new ArrayList<CategoryKeyITO>();
		    	if(modObj.getAddSkills() != null) {
			    	for(String skillRef: modObj.getAddSkills()) {
			    		skills.add(client.getCategoryRequest().getCategoryKeyByReferenceKey(skillRef));
			    	}
		    	}
		    	myUser.setSkills(skills);
		    	
		    	
		      	List<WorkTeamKeyITO> wTeams = new ArrayList<WorkTeamKeyITO>();
		    	if(modObj.getAddWTeams() != null) {
			    	for(String wTeam: modObj.getAddWTeams()) {
			    		wTeams.add(getWorkTeamByRefKey(wTeam));
			    	}
		    	}
		    	myUser.setWorkTeams(wTeams);
		    	
	        	
	        	userRequest.updateUser(myUser);
	        	
	        	results.put(u, "Success");
						
	        }
	       
	        catch (IQServiceClientException e){
	        	System.out.println("Could not found user: "+ u);
	        	e.printStackTrace();
	        	
	        	results.put(u, "Error: Could not find user");
	             	
	        }
		}
			
			modObj.setResults(results);
			return modObj;

	}

	

}
	
